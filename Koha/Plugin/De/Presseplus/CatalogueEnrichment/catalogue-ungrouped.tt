[% INCLUDE 'doc-head-open.inc' %]
<title>Koha &rsaquo; Presseplus plugin &rsaquo; Enrich your catalogue</title>
[% INCLUDE 'doc-head-close.inc' %]
<link rel="stylesheet" type="text/css" href="/api/v1/contrib/presseplus/static/css/presseplus.css" />
</head>

<body id="plugin_presseplus" class="catalog">
    [% INCLUDE 'header.inc' %]
    [% INCLUDE 'cat-search.inc' %]

    <div id="breadcrumbs">
        <a href="/cgi-bin/koha/mainpage.pl">Home</a>
        &rsaquo;
        <a href="/cgi-bin/koha/plugins/plugins-home.pl">Plugins</a>
        &rsaquo;
        Presseplus plugin
    </div>

    <div class="main container-fluid">
    <div class="row">
        <div class="col-sm-8 col-sm-push-2">
            <main>
                <div class="row">

                    <h1>Catalogue enrichment from Presseplus</h1>
                    [% IF errors.size %]
                        <div class="dialog alert">
                            [% FOREACH m IN errors  %]
                                [% SWITCH m.code %]
                                [% CASE 'error_on_retrieve_info' %]
                                    Something wrong happened, the information have not been retrieved correctly: [% m.error | html %]
                                [% CASE 'error_on_retrieve_image' %]
                                    Something wrong happened when retrieving the cover image.
                                [% CASE 'error_on_retrieve_toc_image' %]
                                    Something wrong happened when retrieving the cover table of content image.
                                [% CASE %]Unkown error: [% m.code | html %]
                                [% END %]
                            </div>
                        [% END %]
                    [% END %]

                    [% IF new_biblio %]
                        <fieldset class="rows">
                            <h2>New bibliographic record added with success!</h2>
                            [% IF messages.size %]
                                <ul>
                                [% FOREACH m IN messages %]
                                    <li style="list-style-type:none;">
                                        [% SWITCH m.code %]
                                        [% CASE 'success_on_retrieve_info' %]
                                            <i class="fa fa-fw fa-check"></i> The information have been retrieved from Presseplus.
                                        [% CASE 'success_on_retrieve_image' %]
                                            <i class="fa fa-fw fa-check"></i> One cover image has been found.
                                        [% CASE 'success_on_retrieve_toc_image' %]
                                            <i class="fa fa-fw fa-check"></i> One cover image for the table of content has been found.
                                        [% CASE 'no_toc_image' %]
                                            No image for the table of content found.
                                        [% CASE 'no_cover_image' %]
                                            No cover image found.
                                        [% END %]
                                    </li>
                                [% END %]
                                </ul>
                            [% END %]

                            <a href="/cgi-bin/koha/catalogue/detail.pl?biblionumber=[% new_biblio.biblionumber | uri %]">Go to the bibliographic record</a>
                        </fieldset>
                    [% END %]

                    <fieldset class="rows">
                        <legend>Create a new bibliographic record</legend>
                        <form method="post" class="validated">
                            <input type="hidden" name="class" value="[% CLASS | html %]"/>
                            <input type="hidden" name="method" value="[% METHOD | html %]"/>

                            <label for="issn_ean" class="required">ISSN or EAN: </label>
                            <input type="text" name="issn_ean" value="" required="required" class="required" />

                            <br/>

                            <label for="release_code" class="required">Release code: </label>
                            <input type="text" name="release_code" value="" required="required" class="required" />

                            <br/>

                            <input name="submitted" type="submit" value="Submit" />
                        </form>
                    </fieldset>
                </div>
            </main>
        </div>
    </div>

[% INCLUDE 'intranet-bottom.inc' %]
